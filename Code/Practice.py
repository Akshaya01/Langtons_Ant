import pygame

pygame.mixer.init()
pygame.mixer.music.load("gamesound_2.wav")
pygame.mixer.music.play(-1)

pygame.init()
screen = pygame.display.set_mode((800, 600))
pygame.display.set_caption("Langton's Ant")
screen.fill((255, 255, 255))
pygame.display.update()

image = pygame.image.load("Ants.png").convert()
rect = image.get_rect()
rect.center = (800 // 2, 600 // 2)


# constants
aqua = (0,128,128)
teal = (0,255,255)
white = (255, 255, 255)
black = (0, 0, 0)
UP = 0
RIGHT = 1
DOWN = 2
LEFT = 3

line_width = 10
width = 790
height = 590


class Ant:
    def __init__(self, x, y, color, direction=0):
        self.AntX = x
        self.AntY = y
        self.color = color
        self.direction = direction
        self.steps = 1

    def invertColor(self):  # to invert color of current position
        if self.color == black:
            # screen.set_at((self.AntX, self.AntY), white)
            for i in range(self.AntX, self.AntX + 4):
                for j in range(self.AntY, self.AntY + 4):
                    screen.set_at((i, j), white)
            pygame.display.update()
        else:
            # screen.set_at((self.AntX, self.AntY), black)
            for i in range(self.AntX, self.AntX + 4):
                for j in range(self.AntY, self.AntY + 4):
                    screen.set_at((i, j), black)
            pygame.display.update()

    def updatePixColor(self):  # Updates the color of current pixel
        for i in range(self.AntX, self.AntX + 4):
            for j in range(self.AntY, self.AntY + 4):
                self.color = screen.get_at((i, j))

    def updateDirection(self):  # to change the direction depending on color
        if self.color == black:
            if self.direction == 0:
                self.direction = 3
            else:
                self.direction -= 1
        else:
            if self.direction == 3:
                self.direction = 0
            else:
                self.direction += 1

    def updateCoordinates(self):  # to update the coordinates to next position
        if self.direction == LEFT:
            self.AntX -= 5#1
        elif self.direction == UP:
            self.AntY -= 5#1
        elif self.direction == RIGHT:
            self.AntX += 5#1
        else:
            self.AntY += 5#1

    def current_location_2_red(self):  # indicating ant position with red color
        for i in range(self.AntX, self.AntX + 4):
            for j in range(self.AntY, self.AntY + 4):
                screen.set_at((i, j), (255, 0, 0))


def move(ant1: Ant):  # to continue the movement of ant
    pygame.display.update()
    ant1.current_location_2_red()
    ant1.updateDirection()
    ant1.invertColor()
    ant1.updateCoordinates()
    ant1.steps = ant1.steps + 1
    ant1.updatePixColor()


def showSteps(ant1: Ant):  # to display the number of steps
    font = pygame.font.Font('freesansbold.ttf', 32)
    steps = font.render("Steps :" + str(ant1.steps), True, black)
    screen.fill(white, (0, 0, 250, 70))
    screen.blit(steps, (10, 10))
    pygame.display.flip()


running = True

while running:
    for i in range(0, 3):

        screen.blit(image, rect)
        pygame.display.update()
        pygame.time.delay(250)
        screen.fill(aqua) ## fill the screen with aqua
        # top border
        pygame.draw.rect(screen, teal, [0, 0, width, line_width])
        # bottom border
        pygame.draw.rect(screen, teal, [0, height, width, line_width])
        # left border
        pygame.draw.rect(screen, teal, [0, 0, line_width, height])
        # right border
        pygame.draw.rect(screen, teal, [width, 0, line_width, height + line_width])

    pygame.display.update()

    events = pygame.event.get()
    for event in events:
        if event.type == pygame.QUIT:
            running = False
        elif event.type == pygame.MOUSEBUTTONDOWN:

            mouseX, mouseY = pygame.mouse.get_pos()
            A1 = Ant(mouseX, mouseY, (0, 0, 0))
            move(A1)
            while event.type != pygame.QUIT:
                events = pygame.event.get()
                pygame.time.delay(250)
                for event in events:
                    if event.type == pygame.QUIT:
                        break
                showSteps(A1)
                # modified the available screen area according to the counter frame
                if A1.AntX in range(0, 230) and A1.AntY in range(0, 50):
                    break
                elif A1.AntX < 775 and A1.AntY < 575:
                    move(A1)
                else:
                    break
            running = False
        else:
            pass
