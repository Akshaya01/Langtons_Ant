import tkinter as tk
from tkinter import *
from tkinter import messagebox
from tkinter import simpledialog
import sys
import pygame
from pygame import *
pygame.init()
root = Tk()# Initializing Tkinter...

# constants
white = (255, 255, 255)
black = (0, 0, 0)
aqua = (0, 128, 128)
teal = (0, 255, 255)
lightGreen = (153, 255, 153)
pink = (204, 0, 102)
grey = (192, 192, 192)
UP = 0
RIGHT = 1
DOWN = 2
LEFT = 3
line_width = 10
width = 790
height = 590
font = pygame.font.SysFont('Corbel', 32)

# Creating a menu bar
menubar = Menu(root)
root.config(menu=menubar)

# Create sub menu
subMenu = Menu(menubar, tearoff=0)

def about_us():
    tk.messagebox.showinfo('About Us', 'Langtons Ant game is created by Team6')

def about_game():
    tk.messagebox.showinfo('About Game', "Squares on a plane are colored variously either black or white. We arbitrarily identify one square as the 'ant'. The ant can travel in any of the four cardinal directions at each step it takes. The 'ant' moves according to the rules below:"
                                         "\n\n- At a white square, turn 90° clockwise, flip the color of the square, move forward one unit."
                                         "\n\n- At a black square, turn 90° counter-clockwise, flip the color of the square, move forward one unit."
                                         "\n\n- Langton's ant can also be described as a cellular automaton.")

menubar.add_cascade(label="File", menu=subMenu)
subMenu.add_command(label="About us", command = about_us )
subMenu.add_command(label="About Game", command = about_game)

root.geometry('800x600')
root.title("Langton's Ant")
root['background'] = 'teal'

def play_sound():
    # Adding sound effects
    pygame.mixer.init()
    pygame.mixer.music.load("gamesound_2.wav")
    pygame.mixer.music.play(-1)
play_sound()

muted = FALSE

def mute_music():
    global muted
    if muted:
        pygame.mixer.music.set_volume(0.5)
        sound_btn.configure(image=sound_photo)
        scale.set(50)
        muted = FALSE
    else:
        pygame.mixer.music.set_volume(0)
        sound_btn.configure(image=mute_photo)
        scale.set(0)
        muted = TRUE

def set_volume(val):
    volume = int(val) / 100
    pygame.mixer.music.set_volume(volume)

mute_photo = PhotoImage(file='mute.png')
sound_photo = PhotoImage(file='sound.png')
sound_btn = Button(root, image=sound_photo, command=mute_music)
sound_btn.place(relx = 1, x =-2, y = 2, anchor = NE)

scale = Scale(root, from_=0, to=100, orient=HORIZONTAL, command=set_volume)
scale.set(50)
pygame.mixer.music.set_volume(0.5)
scale.place(relx = 1, x =-2, y = 50, anchor = NE)
play_image = PhotoImage(file = 'Play_Button.PNG')
play_btn = Button(root, image = play_image, command = root.destroy)
play_btn.place(relx = 1, x =-300, y = 300, anchor = NE)

root.mainloop()

# Display the screen
pygame.init()
screen = pygame.display.set_mode((800, 600))
pygame.display.set_caption("Langton's Ant")
screen.fill((255, 255, 255))
pygame.display.update()

# simulation screen
image = pygame.image.load("Ants.png").convert()
rect = image.get_rect()
rect.center = (800 // 2, 600 // 2)

class Ant:
    def __init__(self, x, y, color, direction=0):
        self.AntX = x
        self.AntY = y
        self.color = color
        self.direction = direction
        self.steps = 0

    def invertColor(self):  # to invert color of current position
        if self.color == black:
            for i in range(self.AntX, self.AntX + 4):
                for j in range(self.AntY, self.AntY + 4):
                    screen.set_at((i, j), white)
            pygame.display.update()
        else:
            for i in range(self.AntX, self.AntX + 4):
                for j in range(self.AntY, self.AntY + 4):
                    screen.set_at((i, j), black)
            pygame.display.update()
    
    def updatePixColor(self):      # Updates the color of current pixel
            self.color = screen.get_at((self.AntX , self.AntY)) 

    def updateDirection(self):  # to change the direction depending on color
        if self.color == black:
            if self.direction == 0:
                self.direction = 3
            else:
                self.direction -= 1
        else:
            if self.direction == 3:
                self.direction = 0
            else:
                self.direction += 1

    def updateCoordinates(self):  # to update the coordinates to next position
        if self.direction == LEFT:
            self.AntX -= 5
        elif self.direction == UP:
            self.AntY -= 5
        elif self.direction == RIGHT:
            self.AntX += 5
        else:
            self.AntY += 5

    def current_location_2_red(self):  # indicating ant position with red color
        for i in range(self.AntX, self.AntX + 4):
            for j in range(self.AntY, self.AntY + 4):
                screen.set_at((i, j), (255, 0, 0))

def move(ant1: Ant):  # to continue the movement of ant
    pygame.display.update()
    ant1.updateDirection()
    ant1.invertColor()
    ant1.updateCoordinates()
    ant1.steps = ant1.steps + 1
    ant1.updatePixColor()

def moveIfInRange(A1 : Ant):      # move the ant if it is in range of available screen area
    if A1.AntX in range(0,250) and A1.AntY in range(0, 70):
        steps_window(A1)
        pygame.quit()
        sys.exit()
    elif A1.AntX  <5 or A1.AntY < 5:
        steps_window(A1)
        pygame.quit()
        sys.exit()
    elif (A1.AntX + 5) < 775 and (A1.AntY + 5) < 575:
        A1.current_location_2_red()
        pygame.display.update()
        move(A1)
        showSteps(A1)
    else:
        steps_window(A1)
        pygame.quit()
        sys.exit()

def showSteps(ant1: Ant):         # to display the number of steps 
        font = pygame.font.Font('freesansbold.ttf',32)
        steps = font.render("Steps :" + str(ant1.steps), True, black)
        screen.fill(aqua, (0, 0, 250, 70))
        pygame.draw.rect(screen, teal, [0, 0, 250, line_width])
        pygame.draw.rect(screen, teal, [0, 0, line_width, 70])
        screen.blit(steps, (13, 13))
        pygame.display.flip()

def interface():                 # Game interface 
        screen.blit(image, rect)
        pygame.display.update()
        pygame.time.delay(250)
        screen.fill(aqua)        # fill the screen with aqua
        # top border
        pygame.draw.rect(screen, teal, [0, 0, width, line_width])
        # bottom border
        pygame.draw.rect(screen, teal, [0, height, width, line_width])
        # left border
        pygame.draw.rect(screen, teal, [0, 0, line_width, height])
        # right border
        pygame.draw.rect(screen,teal, [width, 0, line_width, height + line_width])

def ensure():                   # Ensure if the user really wants to quit           
        window = Tk()
        window.eval('tk::PlaceWindow %s center' %window.winfo_toplevel())
        window.withdraw()
        if messagebox.askyesno('Window', 'Do you want to exit') == True:
            window.destroy()
            window.quit()
            steps_window(A1)
            pygame.quit()
            sys.exit()
        else:
            window.destroy()
            window.quit()

def steps_window(ant1: Ant):
    window = Tk()
    window.eval('tk::PlaceWindow %s center' % window.winfo_toplevel())
    window.withdraw()
    messagebox.showinfo("Langton's Ant", 'Steps: ' + str(ant1.steps))
    window.destroy()
    window.quit()

#for selecting Ant's initial direction
application_window = tk.Tk()
application_window.withdraw()
answer = simpledialog.askstring("Input", "Input the direction of your Ant: ",parent=application_window)
if answer is not None:
    directionStr = answer.upper()
else:
    print("Please enter the direction to proceed")

def convert(directionStr): #To convert the Direction which is in String Format
    if directionStr == "UP":
        return 0
    elif directionStr == "RIGHT":
        return 1
    elif directionStr == "DOWN":
        return 2
    elif directionStr == "LEFT":
        return 3
    else:
        return -1

running = True

while running:
    interface()
    for event in pygame.event.get():
        if event.type == pygame.QUIT:
            ensure()
        elif event.type == pygame.MOUSEBUTTONDOWN:
            mouseX, mouseY = pygame.mouse.get_pos()
            A1 = Ant(mouseX, mouseY, (0, 0, 0),convert(directionStr))
            A1.current_location_2_red()
            pygame.display.update()
            showSteps(A1)
            pygame.display.update()
            move(A1)
            while running:
                for event in pygame.event.get():
                    if event.type == pygame.QUIT:
                        ensure()
                showSteps(A1)
                moveIfInRange(A1)
                   
        else:
            pass
