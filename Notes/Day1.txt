On Day1 we have installed the softwares used to build Langton's Ant  
Installed softwares :
->Debian
->Python 3
->Pycharm
->Pygame module
->Texmaker

Started exploring Pygame and executed basic codes in debian using Pygame.

Refernce Links : 
-> https://docs.python.org/
-> https://www.overleaf.com/learn
-> https://youtu.be/FfWpgLFMI7w 

Challenges faced :
-> faced an issue while installing Texmaker in PC 
     ->Referred link - "https://youtu.be/uKetjJTDSqk"
-> unable to work on Texmaker - while executing it is popping out an error message
     -> to overcome this issue we searched in various websites but unable to resolve the error. We tried to uninstall and install the Texmaker and set the path vaariable and it worked.
-> we faced merge conflicts in GitLab while merging files 
     -> to overcome this issue we have looked into different videos and websites
          ->Reffered Link - "https://youtu.be/sgzkY5vFKQQ"
->installtion of vim in debian 
     -> Reffered link - "https://www.cyberciti.biz/faq/howto-install-vim-on-ubuntu-linux/"
-> installation of Git in debian 
     ->Reffered link - "https://www.cyberciti.biz/faq/howto-install-vim-on-ubuntu-linux/"
