On day 6 we have worked to create an interactive gaming interface for the Langton's Ant(Simulation/Pattern formation).
-> Our initial interface had no dialog box asking for inputting the initial direction of the ant from the user which
   we had later added to the existing interface.
-> We tried inputting the direction in the form of a numerical value which we then modified its working so as to take
   inputs even in the form of a string.
-> We had also written the code to modify the range of the available screen area for the movement of Langton's Ant, so that
   if the ant is within the region of permitted movement then it is allowed to move.
-> We have added an additional window that displays the no of steps that the ant has taken so far during its pattern
   formation.
-> Added mute and unmute button.
-> Added play button and also menubar.

Referred Links:
-> https://stackoverflow.com/questions/47799765/extra-tkinter-gui-popup/47799945
-> https://runestone.academy/runestone/books/published/thinkcspy/GUIandEventDrivenProgramming/02_standard_dialog_boxes.html
-> https://docs.python.org/3/library/tkinter.messagebox.html

Learnings:
-> We have worked with Tkinter commands for adding the Dialog boxes for displaying the no of steps, for inputting the
   initial direction of the ant, and a dialogue box if the user really wants to quit the game window.
   https://docs.python.org/3/library/tkinter.html
   https://docs.python.org/3/library/tkinter.messagebox.html

Challenges faced:
-> We have faced problems while displaying the dialog window for inputting direction for the ant, during which an extra
   window was popping out parallely with the actual dialog box.
-> Referred links :
   https://stackoverflow.com/questions/47799765/extra-tkinter-gui-popup/47799945
   https://runestone.academy/runestone/books/published/thinkcspy/GUIandEventDrivenProgramming/02_standard_dialog_boxes.html
   -> To overcome this we have used Tkinter command root.withdraw() for removing the additional window.
-> The issue we faced with is to insert mute and unmute button.
   -> To overcome this we have created a function to change to mute or unmute button at same position.
